## 微信网页一键跳转App

### 1. 注册微信·开放平台，绑定移动应用AppId（配置开发信息 iOS端和Android端），配置关联信息（JS接口安全域名，移动应用AppId，移动应用名称）。


### 2. 登录微信公众平台，用公众号的AppId给后端（崔福圆同学）调用微信接口获取签名等参数。


### 3. H5页面按钮中用的AppId是移动应用AppId。
```
Javascript
<wx-open-launch-app
  id="launch-btn"
  appid="wx5da70a5910f6ec34"
  extinfo=""
>
  <template>
    <button class="btn">App内查看</button>
  </template>
</wx-open-launch-app>
```

### 4. 参考文档

1）开放表情文档 https://developers.weixin.qq.com/doc/oplatform/Mobile_App/WeChat_H5_Launch_APP.html
2）附录5-常见错误及解决方法：https://developers.weixin.qq.com/doc/offiaccount/OA_Web_Apps/JS-SDK.html#66

### 5. demo地址（iOS可以了，安卓开放平台配置有问题）
